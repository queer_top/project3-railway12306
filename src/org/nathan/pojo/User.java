package org.nathan.pojo;

import org.nathan.anno.ColName;

/**
 * 文件名称：@title: User
 * 项目名称：@projectName: railway
 * 项目作者：@author: Nathan_Queer——Top
 * 项目描述：@description:用户信息对象
 * 创建日期：@date: 2024/4/14 17:30
 */

public class User {

    @ColName("u_id")
    private Integer userId;// 用户id

    @ColName("u_nickname")
    private String nickName;// 用户昵称

    @ColName("u_password")
    private String password;// 用户密码

    @ColName("u_username")
    private String userName;// 用户姓名

    @ColName("u_idnumber")
    private String idNum;// 身份证号

    @ColName("u_email")
    private String email;// 邮箱

    @ColName("u_phone")
    private String phoneNum;// 手机号

    // 无参
    public User() {
    }

    // 有参
    public User(Integer userId, String nickName, String password, String userName, String idNum, String email,
                String phoneNum) {
        this.userId = userId;
        this.nickName = nickName;
        this.password = password;
        this.userName = userName;
        this.idNum = idNum;
        this.email = email;
        this.phoneNum = phoneNum;
    }

    // get和set
    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getIdNum() {
        return idNum;
    }

    public void setIdNum(String idNum) {
        this.idNum = idNum;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNum() {
        return phoneNum;
    }

    public void setPhoneNum(String phoneNum) {
        this.phoneNum = phoneNum;
    }

    // 测试
    @Override
    public String toString() {
        return "User{" +
                "userId=" + userId +
                ", nickName='" + nickName + '\'' +
                ", password='" + password + '\'' +
                ", userName='" + userName + '\'' +
                ", idNum='" + idNum + '\'' +
                ", email='" + email + '\'' +
                ", phoneNum='" + phoneNum + '\'' +
                '}';
    }
}
