package org.nathan.utils;

import com.alibaba.druid.pool.DruidDataSourceFactory;

import javax.sql.DataSource;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

/**
 * 文件名称：@title: utils
 * 项目名称：@projectName: railway
 * 项目作者：@author: Nathan_Queer——Top
 * 项目描述：@description:JDBC工具
 * 创建日期：@date: 2024/4/14 17:30
 */
public class JDBCUtils {

    private static DataSource ds;

    static {
        InputStream resourceAsStream = JDBCUtils.class.getClassLoader().getResourceAsStream("db.properties");
        Properties properties = new Properties();
        try {
            properties.load(resourceAsStream);
            ds = DruidDataSourceFactory.createDataSource(properties);
        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    // 定义公有的得到数据源的方法
    public static DataSource getDataSource() {
        return ds;
    }

    // 创建连接
    public static Connection createConn() throws SQLException {
        return ds.getConnection();
    }

    // 关闭连接
    public static void closeConnect(Connection connection, PreparedStatement statement, ResultSet rs) {
        try {
            if (connection != null) {
                connection.close();
            }
            if (statement != null) {
                statement.close();
            }
            if (rs != null) {
                rs.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}

//    private static final String DB_CONFIG_FILE = "db.properties";
//    private static DruidDataSource ds = new DruidDataSource();
// 5.定义关闭资源的方法
//    public static void close(Connection conn, Statement stmt, ResultSet rs) {
//        if (rs != null) {
//            try {
//                rs.close();
//            } catch (SQLException e) {}
//        }
//
//        if (stmt != null) {
//            try {
//                stmt.close();
//            } catch (SQLException e) {}
//        }
//
//        if (conn != null) {
//            try {
//                conn.close();
//            } catch (SQLException e) {}
//        }
//    }
//
//    // 6.重载关闭方法
//    public static void closeConnect(Connection conn, Statement stmt) {
//        close(conn, stmt, null);
//    }

//    static {
//        Properties properties = new Properties();
//        try (FileInputStream fis = new FileInputStream(DB_CONFIG_FILE)) {
//            properties.load(fis);
//            ds.setUrl(properties.getProperty("url"));
//            ds.setUsername(properties.getProperty("username"));
//            ds.setPassword(properties.getProperty("password"));
//            ds.setInitialSize(new Integer(properties.getProperty("initialSize")));
//            ds.setMaxActive(new Integer(properties.getProperty("maxActive")));
//            ds.setMinIdle(new Integer(properties.getProperty("minIdle")));
//            ds.setMaxWait(new Long(properties.getProperty("maxWait")));
//        } catch (Exception e) {
//            e.printStackTrace();
//            throw new RuntimeException("创建连接池失败：" + e.getMessage());
//        }
//
//    }
//        try {
//            return ds.getConnection();
//        } catch (Exception e) {
//            e.printStackTrace();
//            throw new RuntimeException("获取连接失败:" + e.getMessage());
//        }
