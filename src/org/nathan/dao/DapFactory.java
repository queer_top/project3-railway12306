package org.nathan.dao;

import org.nathan.daoImpl.UserDaoImpl;

import java.util.HashMap;
import java.util.Map;

/**
 * 文件名称：@title: DapFactory
 * 项目名称：@projectName: railway
 * 项目作者：@author: Nathan_Queer——Top
 * 项目描述：@description:Dao层工厂
 * 创建日期：@date: 2024/4/14 18:10
 */
public class DapFactory {

    private static Map<Class, Object> daoMap = new HashMap<>();

    static{
        daoMap.put(UserDao.class,new UserDaoImpl());
    }

    public static <T> T getDao(Class cl) {
        return (T) daoMap.get(cl);
    }
}
