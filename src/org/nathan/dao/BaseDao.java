package org.nathan.dao;

import org.nathan.anno.ColName;
import org.nathan.utils.JDBCUtils;

import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 文件名称：@title: BaseDap
 * 项目名称：@projectName: mooc
 * 项目作者：@author: Nathan_Queer——Top
 * 项目描述：@description:简单的增删改查
 * 创建日期：@date: 2024/04/14 0:26
 */
public abstract class BaseDao {
    // 增删改
    public int update(String sql, Object... params) {
        Connection conn = null;
        PreparedStatement statement = null;
        try {
            conn = JDBCUtils.createConn();
            statement = conn.prepareStatement(sql);
            System.out.println(Arrays.toString(params) + "----------base27");
            for (int i = 0; i < params.length; i++) {
                statement.setObject(i + 1, params[i]);
            }
            return statement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            JDBCUtils.closeConnect(conn, statement, null);
//            JDBCUtils.closeConnect(conn, statement);
        }
    }

    // 查
    public <T> List<T> select(String sql, Class<T> cl, Object... params) {
        List<T> list = new ArrayList<>();
        Connection conn = null;
        PreparedStatement statement = null;
        ResultSet rs = null;
        try {
            conn = JDBCUtils.createConn();
            statement = conn.prepareStatement(sql);
            for (int i = 0; i < params.length; i++) {
                statement.setObject(i + 1, params[i]);
            }
            System.out.println(statement);
            rs = statement.executeQuery();
            while (rs.next()) {
                T o = cl.newInstance();
                //根据属性名获取数据库中对应字段的值
                Field[] fields = cl.getDeclaredFields();
                for (Field field : fields) {
                    ColName colName = field.getAnnotation(ColName.class);
                    Object value = rs.getObject(colName.value());
                    field.setAccessible(true);
                    field.set(o, value);
                }
                list.add(o);
            }
        } catch (SQLException | InstantiationException | IllegalAccessException e) {
            throw new RuntimeException(e);
        } finally {
            JDBCUtils.closeConnect(conn, statement, rs);
        }
        return list;
    }

    public <T> List<T> selectByList(String sql, Class<T> cl, List<Object> params) {
        List<T> list = new ArrayList<>();
        Connection conn = null;
        PreparedStatement statement = null;
        ResultSet rs = null;
        try {
            conn = JDBCUtils.createConn();
            statement = conn.prepareStatement(sql);
            for (int i = 0; i < params.size(); i++) {
                statement.setObject(i + 1, params.get(i));
            }
            System.out.println(statement);
            rs = statement.executeQuery();
            while (rs.next()) {
                T o = cl.newInstance();
                //根据属性名获取数据库中对应字段的值
                Field[] fields = cl.getDeclaredFields();
                for (Field field : fields) {
                    ColName colName = field.getAnnotation(ColName.class);
                    Object value = rs.getObject(colName.value());
                    field.setAccessible(true);
                    field.set(o, value);
                }
                list.add(o);
            }
        } catch (SQLException | InstantiationException | IllegalAccessException e) {
            throw new RuntimeException(e);
        } finally {
            JDBCUtils.closeConnect(conn, statement, rs);
        }
        return list;
    }
}