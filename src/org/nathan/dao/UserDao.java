package org.nathan.dao;

import org.nathan.pojo.User;

/**
 * 文件名称：@title: UserDao
 * 项目名称：@projectName: railway
 * 项目作者：@author: Nathan_Queer——Top
 * 项目描述：@description:用户数据库操作接口
 * 创建日期：@date: 2024/4/14 17:29
 */
public interface UserDao {

    /** 
    * 描述:@Description: 登入判断
    * 参数:@Param: [idNum, password]
    * 返回:@return: org.nathan.pojo.User
    * 作者:@Author: Nathan_Queer——Top
    * 日期:@Date: 2024/4/14
    */
    User queryUserByIdNumAndPassword(String idNum,String password);

    /** 
    * 描述:@Description: 注册前判断身份证是否存在
    * 参数:@Param: [idNum]
    * 返回:@return: boolean
    * 作者:@Author: Nathan_Queer——Top
    * 日期:@Date: 2024/4/14
    */
    boolean queryUserByIdNum(String idNum);

    /** 
    * 描述:@Description: 注册
    * 参数:@Param: [user]
    * 返回:@return: int
    * 作者:@Author: Nathan_Queer——Top
    * 日期:@Date: 2024/4/14
    */
    int register(User user);
}
