package org.nathan.dto;

import java.util.HashMap;
import java.util.Map;

/**
 * 文件名称：@title: Result
 * 项目名称：@projectName: railway
 * 项目作者：@author: Nathan_Queer——Top
 * 项目描述：@description:返回结果集
 * 创建日期：@date: 2024/4/14 17:30
 */
public class Result {
    private int statusCode = 200;// 返回状态码200正确，500错误
    private String errMsg;// 返回信息
    private Map<String, Object> data = new HashMap<>();// 返回数据

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public void setErrMsg(String errMsg) {
        this.errMsg = errMsg;
    }

    public Map<String, Object> getData() {
        return data;
    }

    public void setData(Map<String, Object> data) {
        this.data = data;
    }
}
