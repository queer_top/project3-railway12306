package org.nathan.service.serviceImpl;

import org.nathan.dao.DapFactory;
import org.nathan.dao.UserDao;
import org.nathan.dto.Result;
import org.nathan.pojo.User;
import org.nathan.pojo.VCode;
import org.nathan.service.UserService;

/**
 * 文件名称：@title: UserServiceImpl
 * 项目名称：@projectName: railway
 * 项目作者：@author: Nathan_Queer——Top
 * 项目描述：@description:实现用户服务层接口
 * 创建日期：@date: 2024/4/14 18:01
 */
public class UserServiceImpl implements UserService {
    @Override
    public Result login(String vid, String vCode, String idNum, String password) {
        Result result = new Result();
        // 验证码校验
        String saveCode = VCode.vCodeMap.get(vid);
        System.out.println("获取的验证码：" + vCode + "===" + "保存的验证码" + saveCode);
        if (!vCode.equals(saveCode)) {
            result.setStatusCode(500);
            result.setErrMsg("验证码有误");
            return result;
        }
        // 数据库操作
        UserDao userDao = DapFactory.getDao(UserDao.class);
        User user = userDao.queryUserByIdNumAndPassword(idNum, password);
        if (user != null) {
            result.getData().put("user", user);
        } else {
            result.setStatusCode(500);
            result.setErrMsg("登入信息有误");
        }
        return result;
    }

    @Override
    public Result register(String nickname, String password, String userName, String idNum, String email,
                           String phone) {
        Result result = new Result();
        User user = new User();
        // 数据库操作
        UserDao userDao = DapFactory.getDao(UserDao.class);
        if (userDao.queryUserByIdNum(idNum)) {
            user.setNickName(nickname);
            user.setPassword(password);
            user.setUserName(userName);
            user.setIdNum(idNum);
            user.setEmail(email);
            user.setPhoneNum(phone);
            int num = userDao.register(user);
            if (num == 0) {
                result.setStatusCode(500);
                result.setErrMsg("注册失败");
            } else {
                result.getData().put("user", user);
                result.setStatusCode(200);
            }
        } else {
            result.setStatusCode(103);
            result.setErrMsg("身份信息已存在");
        }
        return result;
    }
}
