package org.nathan.service;

import org.nathan.dto.Result;

/**
 * 文件名称：@title: UserService
 * 项目名称：@projectName: railway
 * 项目作者：@author: Nathan_Queer——Top
 * 项目描述：@description:用户服务层
 * 创建日期：@date: 2024/4/14 17:56
 */
public interface UserService {

    /**
     * 描述:@Description: 登入的方法
     * 参数:@Param: [vid, vCode, idNum, password]
     * 返回:@return: org.nathan.dto.Result
     * 作者:@Author: Nathan_Queer——Top
     * 日期:@Date: 2024/4/14
     */
    Result login(String vid, String vCode, String idNum, String password);

    /**
    * 描述:@Description: 注册的方法
    * 参数:@Param: [nickname, password, userName, idNum, email, phone]
    * 返回:@return: org.nathan.dto.Result
    * 作者:@Author: Nathan_Queer——Top
    * 日期:@Date: 2024/4/14
    */
    Result register(String nickname, String password, String userName, String idNum, String email, String phone);
}
