package org.nathan.servlet;

import org.nathan.pojo.VCode;
import org.nathan.utils.CaptchaUtil;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * 文件名称：@title: VCodeServlet
 * 项目名称：@projectName: railway
 * 项目作者：@author: Nathan_Queer——Top
 * 项目描述：@description:验证码
 * 创建日期：@date: 2024/4/14 18:24
 */

@WebServlet("/VerCode")
public class VCodeServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.doPost(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        CaptchaUtil captcha = new CaptchaUtil();
        String codeId = req.getParameter("vid");
        String code = captcha.getCode();
        System.out.println("验证码id：" + codeId + "===" + "验证码" + code);

        // 存入验证码
        VCode.vCodeMap.put(codeId, code);
        System.out.println(VCode.vCodeMap + "============42");
        BufferedImage bufferedImage = captcha.getImage();
        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        //将图片写入bos字节数组输出
        ImageIO.write(bufferedImage, "png", bos);

        // 从字节数组输出流中获取字节数组
        byte[] data = bos.toByteArray();
        resp.setContentType("image/png");

        // 获取响应的字节输出流
        ServletOutputStream outputStream = resp.getOutputStream();

        // 将字节数组写入到响应输出流中
        outputStream.write(data);
    }
}
