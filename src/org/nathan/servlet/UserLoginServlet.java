package org.nathan.servlet;

import com.alibaba.fastjson.JSON;
import org.nathan.dto.Result;
import org.nathan.service.ServiceFactory;
import org.nathan.service.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 文件名称：@title: UserLoginServlet
 * 项目名称：@projectName: railway
 * 项目作者：@author: Nathan_Queer——Top
 * 项目描述：@description:登入
 * 创建日期：@date: 2024/4/14 17:21
 */
public class UserLoginServlet extends HttpServlet {


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String vid = req.getParameter("vid");
        String vCode = req.getParameter("vCode");
        String idNumber = req.getParameter("idNumber");
        String password = req.getParameter("password");

        // 处理
        UserService userService = ServiceFactory.getService(UserService.class);
        Result result = userService.login(vid,vCode,idNumber,password);

        // 响应
        String s = JSON.toJSONString(result);
        resp.getOutputStream().write(s.getBytes("utf-8"));
//        resp.getWriter().print(s);
    }
}
