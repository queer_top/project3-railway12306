package org.nathan.servlet;

import com.alibaba.fastjson.JSON;
import org.nathan.dto.Result;
import org.nathan.service.ServiceFactory;
import org.nathan.service.UserService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 文件名称：@title: UserRegisterServlet
 * 项目名称：@projectName: railway
 * 项目作者：@author: Nathan_Queer——Top
 * 项目描述：@description:注册
 * 创建日期：@date: 2024/4/14 17:22
 */

@WebServlet("/register")
public class UserRegisterServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.doPost(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String nikeName = req.getParameter("nikename");
        String password = req.getParameter("password");
        String username = req.getParameter("username");
        String idnNum = req.getParameter("idnumber");
        String email = req.getParameter("email");
        String phone = req.getParameter("phone");

        // 处理
        UserService userService = ServiceFactory.getService(UserService.class);
        Result result = userService.register(nikeName,password,username,idnNum,email,phone);

        // 响应
        String s = JSON.toJSONString(result);
        resp.getOutputStream().write(s.getBytes("utf-8"));
//        resp.getWriter().print(s);
    }
}