package org.nathan.daoImpl;

import org.nathan.dao.BaseDao;
import org.nathan.dao.UserDao;
import org.nathan.pojo.User;

import java.util.List;

/**
 * 文件名称：@title: UserDaoImpl
 * 项目名称：@projectName: railway
 * 项目作者：@author: Nathan_Queer——Top
 * 项目描述：@description:实现用户数据库操作接口
 * 创建日期：@date: 2024/4/14 17:30
 */
public class UserDaoImpl extends BaseDao implements UserDao {
    @Override
    public User queryUserByIdNumAndPassword(String idNum, String password) {
        String sql = "select * from t_user where u_idnumber = ? and u_password = ?";
        List list = super.select(sql, User.class, idNum, password);
        if (list.size() != 0) {
            return (User) list.get(0);
        } else {
            return null;
        }
    }

    @Override
    public boolean queryUserByIdNum(String idNum) {
        String sql = "select * from t_user where u_idnumber = ?";
        List list = super.select(sql, User.class, idNum);
        if (list.size() == 0) {
            // 身份证不存在
            return true;
        } else {
            // 身份证已存在
            return false;
        }
    }

    @Override
    public int register(User user) {
        String sql = "insert into t_user (u_nickname,u_password,u_username,u_idnumber,u_email,u_phone)values(?,?,?,?," +
                "?,?);";
        return super.update(sql, user.getNickName(), user.getPassword(), user.getUserName(), user.getIdNum(),
                user.getEmail(), user.getPhoneNum());
    }
}
