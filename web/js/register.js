// 用户名正则6-30位字母、数字或“_”,字母开头
var nicknameRegular = /^[a-zA-Z][a-zA-Z0-9_]{5,29}$/;
// 密码正则6-20位字母、数字或符号
var pwdRegular = /^[a-zA-Z0-9!@#$%^&*()_+]{6,20}$/;
// 手机号正则
var telRegular = /^(13[0-9]|14[01456879]|15[0-35-9]|16[2567]|17[0-8]|18[0-9]|19[0-35-9])\d{8}$/;
// 邮箱正则
var emailRegular = /^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
// 身份证正则
var idNumberRegular = /^\d{6}((((((19|20)\d{2})(0[13-9]|1[012])(0[1-9]|[12]\d|30))|(((19|20)\d{2})(0[13578]|1[02])31)|((19|20)\d{2})02(0[1-9]|1\d|2[0-8])|((((19|20)([13579][26]|[2468][048]|0[48]))|(2000))0229))\d{3})|((((\d{2})(0[13-9]|1[012])(0[1-9]|[12]\d|30))|((\d{2})(0[13578]|1[02])31)|((\d{2})02(0[1-9]|1\d|2[0-8]))|(([13579][26]|[2468][048]|0[048])0229))\d{2}))(\d|X|x)$/;
// 姓名正则 中文名和英文名
var nameRegular = /^[\u4e00-\u9fa5a-zA-Z]+$/;

// 失焦后判断正则
window.onload = function () {
    document.getElementById("username").addEventListener("blur", checkNickname);
    document.getElementById("password").addEventListener("blur", checkPwd);
    document.getElementById("phone").addEventListener("blur", checkTel);
    document.getElementById("email").addEventListener("blur", checkEmail);
    document.getElementById("identity").addEventListener("blur", checkIdNumber);
    document.getElementById("realname").addEventListener("blur", checkRealname);
}

function checkNickname() {
    var nickName = document.getElementById("username").value.trim();
    var falg = nicknameRegular.test(nickName);
    var s_username = document.getElementById("span_username")
    if (falg) {
        s_username.innerHTML = "√"
    } else {
        s_username.innerHTML = "× <br> <font color='red'style='font-size: xx-small'>用户名格式错误(正确格式为6-30位字母、数字或“_”,字母开头)</font>"
    }
    return falg;
}

function checkPwd() {
    var pwd = document.getElementById("password").value.trim();
    var falg = pwdRegular.test(pwd);
    var s_password = document.getElementById("span_password")
    if (falg) {
        s_password.innerHTML = "√"
    } else {
        s_password.innerHTML = "× <br> <font color='red'style='font-size: xx-small'>密码格式错误(正确格式为6-20位字母、数字或符号)</font>"
    }
    return falg;
}

function checkTel() {
    var phone = document.getElementById("phone").value.trim();
    var falg = telRegular.test(phone);
    var s_phone = document.getElementById("span_phone")
    if (falg) {
        s_phone.innerHTML = "√"
    } else {
        s_phone.innerHTML = "× <br> <font color='red'style='font-size: xx-small'>手机号格式错误(请输入标准运营商手机号)</font>"
    }
    return falg;
}

function checkEmail() {
    var email = document.getElementById("email").value.trim();
    var falg = emailRegular.test(email);
    var s_emali = document.getElementById("span_emali")
    if (falg) {
        s_emali.innerHTML = "√"
    } else {
        s_emali.innerHTML = "× <br> <font color='red'style='font-size: xx-small'>邮箱格式错误(请输入标准邮箱)</font>"
    }
    return falg;
}

function checkIdNumber() {
    var idNumb = document.getElementById("identity").value.trim();
    var falg = idNumberRegular.test(idNumb);
    var identity = document.getElementById("span_identity")
    if (falg) {
        identity.innerHTML = "√"
    } else {
        identity.innerHTML = "× <br> <font color='red'style='font-size: xx-small'>身份证号格式错误(请输入正确的身份证号)</font>"
    }
    return falg;
}

function checkRealname() {
    var username = document.getElementById("realname").value.trim();
    var falg = nameRegular.test(username);
    var s_realname = document.getElementById("span_realname")
    if (falg) {
        s_realname.innerHTML = "√"
    } else {
        s_realname.innerHTML = "× <br> <font color='red'style='font-size: xx-small'>姓名格式错误(请输入正确的姓名)</font>"
    }
    return falg;
}

// 跳转登入界面
function toLogin() {
    let nikename = $('#username').val().trim();
    let password = $('#password').val().trim();
    let surepassword = $('#surePassword').val().trim();
    let username = $('#realname').val().trim();
    let idnumber = $('#identity').val().trim();
    let email = $('#email').val().trim();
    let phone = $('#phone').val().trim();
    let selectedValue = $('#province').val();
    if (nikename === "" || password === "" || surepassword === "" || username === "" || idnumber === "" || email === "" || phone === "" || selectedValue === "") {
        alert("请填写完整信息后再注册");
        return;
    }

    if (password !== surepassword) {
        alert("两次密码不一致！");
        return;
    }

    // 浏览器访问服务器，服务器中进行数据库的验证
    $.ajax("register", {
        data: {
            nikename: nikename,
            password: password,
            username: username,
            idnumber: idnumber,
            email: email,
            phone: phone
        },
        type: "post",
        dataType: "JSON",
        success: function (data) {
            if (data.statusCode === 200) {
                alert("注册成功")
                location.href = "login.html";
            } else {
                alert(data.data.errMsg);
            }
        }
    })
}