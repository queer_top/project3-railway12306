var user = JSON.parse(localStorage.getItem('user'));
if (user !== null) {
    let userElement = document.getElementById('user');
    let s = `
    <a href="#">${user.userName}</a>
    <button onclick="toLogin()" style="background-color: rgba(0, 0, 0, 0.1);border-style: hidden;border-radius: 4px;
    width: auto; height: auto; cursor: pointer;">退出登入</button>`;
    userElement.innerHTML = s;
}

// 退出登入
function toLogin() {
    localStorage.removeItem('user');
    alert("账号已退出")
    location.href = "index.html";
}