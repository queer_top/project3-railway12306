// 登入界面轮播图
var i = 1;
var img = document.getElementById("banner");
setInterval(fun, 1000);

function fun() {
    img.src = "./img/banner_" + i + ".jpg" //  ../ 代表当前目录的上一级目录
    i++;
    if (i >= 1) {
        i = 1;
    }
}


// 代开注册界面
function openRegister() {
    location.href = "register.html";
}

// 打开首页的方法
function openHomePage() {
    let vid;
    let idNumber = $('#idnumber').val().trim();
    let password = $('#password').val().trim();
    let vCode = $("#inputCode").val().trim();
    // 前端非空判断
    if (idNumber === "" || password === "") {
        alert('请输入账号密码！');
        return;
    }
    if (vCode === "") {
        alert('请输入验证码！');
        return;
    }

    // 浏览器访问服务器，服务器中进行数据库的验证
    $.ajax("userLogin", {
        data: {
            vid: vid,
            vCode: vCode,
            idNumber:idNumber,
            password:password
        },
        type: "post",
        dataType: "JSON",
        success: function (data) {
            // 释放浏览器本地资源
            localStorage.removeItem("user");
            let user = data.data.user;
            console.log(user);
            if (data.statusCode === 200) {
                alert("登入成功")
                // 将登入用户存入浏览器本地内存
                let userString = JSON.stringify(user);
                localStorage.setItem("user", userString);
                location.href = "index.html";
                window.close();
            } else {
                alert(data.errMsg)
                flushCode();
            }
        }
    })
}

// 刷新验证码的方法
function flushCode() {
    var vid = new Date().getTime();
    $("#Code").prop("src", "VerCode?id=" + vid)
}